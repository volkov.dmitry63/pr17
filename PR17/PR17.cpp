﻿// PR17.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

class Vector
{
public:
	Vector()
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		cout << '\n' << x << ' ' << y << ' ' << z;
	}
	
	int Module()
	{
		//return x * x + y * y + z * z;
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}

	
private:
	double x = 2;
	double y = 3;
	double z = 1;
};


int main()
{
	Vector v, v1;
	v.Show();
	cout << endl << v.Module();

	return 0;
}
